package br.mpj.maplink.repository;

import br.mpj.maplink.entity.Agendamento;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AgendamentoRepository extends CrudRepository<Agendamento, Long> {

    @Query(value = "select c.nome , date_trunc('day',a.data_hora) as d , sum(s.valor) from agendamento a " +
            "cross join servico s, cliente c " +
            "where a.servico_id=s.id and  a.cliente_id = c.id " +
            "group by c.nome, d , s.valor",nativeQuery = true)
    public Object[] sumarizeTodosAgendamentosAgrupadosPorDataEValor();

    @Query("SELECT a FROM Agendamento a WHERE a.cliente.id = :clienteID and a.dataHora= :dataHora ")
    public List<Agendamento> filtarTodosAgendamentosClienteEDataHora(@Param("clienteID") Long clienteID, @Param("dataHora") LocalDateTime dataHora);
}