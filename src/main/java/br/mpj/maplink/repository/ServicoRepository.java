package br.mpj.maplink.repository;

import br.mpj.maplink.entity.Servico;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicoRepository extends CrudRepository<Servico, Long> {

    public Servico findByCodigo(String codigo);

    public void deleteByCodigo(String codigo);
}