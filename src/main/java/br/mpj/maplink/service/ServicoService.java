package br.mpj.maplink.service;

import br.mpj.maplink.entity.Servico;
import br.mpj.maplink.repository.ServicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServicoService {

    @Autowired
    private ServicoRepository servicoRepository;

    public  Servico save(Servico entity) {
        return servicoRepository.save(entity);
    }

    public Iterable<Servico> findAll() {
        return servicoRepository.findAll();
    }

    public void deleteById(Long aLong) {
        servicoRepository.deleteById(aLong);
    }

    public Servico findByCodigo(String codigo){ return servicoRepository.findByCodigo(codigo);}

    public void deleteByCodigo(String codigo){servicoRepository.deleteByCodigo(codigo);}

}
