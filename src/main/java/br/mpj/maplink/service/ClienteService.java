package br.mpj.maplink.service;

import br.mpj.maplink.entity.Cliente;
import br.mpj.maplink.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;


    public Cliente save(Cliente entity) {
        return clienteRepository.save(entity);
    }

    public Iterable<Cliente> findAll() {
        return clienteRepository.findAll();
    }

    public Cliente findById(Long aLong) {
        return clienteRepository.findById(aLong).get();
    }

    public void deleteById(Long aLong) {
        clienteRepository.deleteById(aLong);
    }



}
