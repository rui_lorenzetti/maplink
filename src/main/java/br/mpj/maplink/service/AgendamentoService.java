package br.mpj.maplink.service;

import br.mpj.maplink.entity.Agendamento;
import br.mpj.maplink.entity.Cliente;
import br.mpj.maplink.entity.Servico;
import br.mpj.maplink.repository.AgendamentoRepository;
import br.mpj.maplink.repository.ClienteRepository;
import br.mpj.maplink.repository.ServicoRepository;
import br.mpj.maplink.vo.AgendamentoVO;
import br.mpj.maplink.vo.ReagendamentoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * ● O agendamento deve possuir as operações:
 *      ● Criar um agendamento de serviço para um cliente referente;
 *      ● Remarcar a data de um agendamento;
 *      ● Listar todos agendamentos.
 *      ● Listar todos os agendamentos agrupados por data e valor
 *      ● Listar todos agendamentos.
 *          ● Listar todos os agendamentos agrupados por data e valor
 * ● Deve ser possível realizar a operação de sumarização dos agendamentos, com os seguintes requisitos:
 *      ● Filtros
 *          ■ DataHora
 *          ■ Cliente
 *      ● O retorno deve conter:
 *           ■ Cliente
 *           ■ Data (Dia/Mês/Ano)
 *           ■ Valor total
 */
@Service
public class AgendamentoService {

    @Autowired
    private ServicoRepository servicoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private AgendamentoRepository agendamentoRepository;

    public Agendamento scheduleAgendamento(AgendamentoVO vo) throws  IllegalArgumentException{
        Servico servico = servicoRepository.findByCodigo(vo.getCodigoServico());
        Cliente cliente = clienteRepository.findById(vo.getClienteId()).orElse(new Cliente());
        //aqui deveria ter criado um builder
        Agendamento agendamento = agendamentoRepository.save(new Agendamento(servico,cliente,vo.getDataHoraAgendamento(),vo.getObservacao()));
        return agendamento;
    }

    public void rescheduleAgendamento(ReagendamentoVO reagendamentoVO) throws  IllegalArgumentException{
        Agendamento agendamento = agendamentoRepository.findById(reagendamentoVO.getAgendamentoId()).get();
        agendamento.setDataHora(reagendamentoVO.getNovaDataHora());
        agendamentoRepository.save(agendamento);
    }


    public Iterable<Agendamento> findAll(){
        return agendamentoRepository.findAll();
    }


    public Agendamento findById(Long id){
        return agendamentoRepository.findById(id).orElse(null);
    }

    public  Object[] sumarizeAgendamentos(){
        return  agendamentoRepository.sumarizeTodosAgendamentosAgrupadosPorDataEValor();
    }


}
