package br.mpj.maplink.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "agendamento")
@Entity
public class Agendamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Basic
    private java.time.LocalDateTime dataHora;

    @Lob
    private String observacao;

    @ManyToOne(optional = false)
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @ManyToOne(optional = false)
    @JoinColumn(name = "servico_id")
    private Servico servico;

    public Agendamento(Servico servico, Cliente cliente,LocalDateTime dataHora,String observacao) {
        if(servico == null){
            throw  new IllegalArgumentException("Deve haver um servico para a criacao do agendamento.");
        }
        if(cliente == null){
            throw  new IllegalArgumentException("Deve haver um cliente para a criacao do agedamento");
        }
        if(dataHora == null){
            throw  new IllegalArgumentException("Deve haver a data e hora do agendamento");
        }
        this.servico = servico;
        this.cliente = cliente;
        this.dataHora = dataHora;
        this.observacao = observacao;
    }

    public Agendamento(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

}