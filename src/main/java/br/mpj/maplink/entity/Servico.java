package br.mpj.maplink.entity;


import com.sun.istack.NotNull;

import javax.persistence.*;
import java.math.BigDecimal;


@Table(name = "servico")
@Entity
public class Servico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(columnDefinition = "serial")
    private Long id;

    @Column(nullable = false, unique = true )
    private String codigo;

    @Column(nullable = false)
    private String descricao;

    @Column(name = "valor", precision = 20, scale = 2, nullable = false)
    @NotNull
    private BigDecimal valor;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}