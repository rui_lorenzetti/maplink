package br.mpj.maplink.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import br.mpj.maplink.entity.Servico;
import br.mpj.maplink.service.ServicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@Transactional
@RequestMapping("/api/v1/servico")
public class ServicoController {

    @Autowired
    private ServicoService servicoService;

    @GetMapping("/")
    public ResponseEntity<Iterable<Servico>> findAll(){
        return new ResponseEntity<>(servicoService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{codigo}")
    public ResponseEntity<Servico> findOne(@PathVariable("codigo") String codigo){
        return new ResponseEntity<Servico>(servicoService.findByCodigo(codigo), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Servico> create(@RequestBody @Valid Servico servico){
            return  new ResponseEntity<>(servicoService.save(servico), HttpStatus.CREATED);
    }

     @DeleteMapping("/{codigo}")
    public ResponseEntity delete(@PathVariable("codigo") String codigo){
            servicoService.deleteByCodigo(codigo);
            return new ResponseEntity(HttpStatus.OK);

    }

}
