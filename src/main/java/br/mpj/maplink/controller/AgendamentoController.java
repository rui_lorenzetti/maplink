package br.mpj.maplink.controller;

import br.mpj.maplink.entity.Agendamento;
import br.mpj.maplink.service.AgendamentoService;
import br.mpj.maplink.vo.AgendamentoVO;
import br.mpj.maplink.vo.ReagendamentoVO;
import br.mpj.maplink.vo.SumarioAgendamentoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * * ● O agendamento deve possuir as operações:
 *       ● Criar um agendamento de serviço para um cliente referente;
 *       ● Remarcar a data de um agendamento;
 *       ● Listar todos agendamentos.
 *       ● Listar todos os agendamentos agrupados por data e valor
 * Deve ser possível realizar a operação de sumarização dos agendamentos, com os seguintes requisitos:
 *       ● Filtros
 *          ■ DataHora
 *          ■ Cliente
 *       ● O retorno deve conter
 *           ■ Cliente
 *           ■ Data (Dia/Mês/Ano)
 *           ■ Valor total
 */
@Controller
@RequestMapping("/api/v1/agendamento")
public class AgendamentoController {


    @Autowired
    private AgendamentoService agendamentoService;

    @GetMapping("/all")
    public ResponseEntity<Iterable<Agendamento>> findAll(){
        return new ResponseEntity<>(agendamentoService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/grouped")
    public ResponseEntity<List<SumarioAgendamentoVO>> findAllGroupedByClienteAndDate(){
        List<SumarioAgendamentoVO> sumarios = new ArrayList<>();
        Object[] objs = agendamentoService.sumarizeAgendamentos();
        Arrays.stream(objs).forEach(o -> {
            String nome = (String) ((Object[]) o)[0];
            Timestamp data = (Timestamp) ((Object[]) o)[1];
            BigDecimal valor = (BigDecimal) ((Object[]) o)[2];
            sumarios.add(new SumarioAgendamentoVO(nome,data.toLocalDateTime().toLocalDate(),valor));
        });
        return new ResponseEntity(sumarios,HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Agendamento> findOne(@PathVariable("id") Long id){
        return new ResponseEntity(agendamentoService.findById(id),HttpStatus.OK);
    }

    @PostMapping(value = "/schedule")
    public ResponseEntity<Agendamento> scheduleAgendamento(@RequestBody @Valid AgendamentoVO agendamentoVO) {
        return new ResponseEntity<>(agendamentoService.scheduleAgendamento(agendamentoVO),HttpStatus.CREATED);
    }

    @PostMapping("/reschedule")
    public  ResponseEntity rescheduleAgendamento(@RequestBody @Valid ReagendamentoVO reagendamento){
         agendamentoService.rescheduleAgendamento(reagendamento);
         return new ResponseEntity(HttpStatus.OK);
    }
}