package br.mpj.maplink.vo;

import java.math.BigDecimal;
import java.time.LocalDate;

public class SumarioAgendamentoVO {
    private String nome;
    private LocalDate data;
    private BigDecimal somaValores;

    public SumarioAgendamentoVO(String nome, LocalDate data, BigDecimal somaValores){
        this.nome = nome;
        this.data = data;
        this.somaValores = somaValores;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public BigDecimal getSomaValores() {
        return somaValores;
    }

    public void setSomaValores(BigDecimal somaValores) {
        this.somaValores = somaValores;
    }
}
